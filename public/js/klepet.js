// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};

var tabelaVzdevkov = [];

/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.spremeniBarvo = function(barva) {
  document.getElementById("sporocila").style.backgroundColor = barva;
  document.getElementById("kanal").style.backgroundColor = barva;
  document.getElementById("poslji-sporocilo").style.backgroundColor = barva;
};

/**
 * Dodajanje vzdevka v tabelo vzdevkov
 * 
 */

Klepet.prototype.dodajVzdevek = function(uporabnikovVzdevek) {
  var jeZe = false;
  for (var i = 0; i < tabelaVzdevkov.length; i++) {
    if (uporabnikovVzdevek.uporabnik == tabelaVzdevkov[i].uporabnik) {
      tabelaVzdevkov[i].uporabnik = uporabnikovVzdevek.uporabnik;
      tabelaVzdevkov[i].vzdevek = uporabnikovVzdevek.vzdevek;
      jeZe = true;
    }
  }
  if (jeZe == false) {
      tabelaVzdevkov.push(uporabnikovVzdevek);
  }
};

/**
 * Sprememba vzdevka
 * 
 */

Klepet.prototype.spremeniVzdevek = function(uporabnik) {
  var prejsnjiVzdevek = uporabnik.prejsnji;
  var naslednjiVzdevek = uporabnik.trenutni;
  for (var i = 0; i < tabelaVzdevkov.length; i++) {
    if (tabelaVzdevkov[i].uporabnik == prejsnjiVzdevek) {
      tabelaVzdevkov[i].uporabnik = naslednjiVzdevek;
    }
  }
};

/**
 * Sprememba vzdevka v besedilu
 * 
 */

Klepet.prototype.vzdevekSporocilo = function(sporocilo) {
    for (var i = 0; i < tabelaVzdevkov.length; i++) {
      if (sporocilo.indexOf(tabelaVzdevkov[i].uporabnik) > -1) {
        sporocilo = 
        sporocilo.split(tabelaVzdevkov[i].uporabnik).join(tabelaVzdevkov[i].vzdevek + " (" + tabelaVzdevkov[i].uporabnik + ")");
        }
    }
    return sporocilo;
};

/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'pridruzitev2':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      if (parametri) {
        for (var i = 0; i < tabelaVzdevkov.length; i++) {
          if (parametri[1] == tabelaVzdevkov[i].vzdevek) {
            parametri[1] = tabelaVzdevkov[i].uporabnik;
          }
        }
        this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3]});
        sporocilo = '(zasebno za ' + parametri[1] + '): ' + parametri[3];
        sporocilo = this.vzdevekSporocilo(sporocilo);
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    case 'preimenuj':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      if (parametri) {
        /*this.socket.emit('sporocilo', {uporabnik: parametri[1], besedilo: parametri[3]});
        sporocilo = '(zasebno za ' + parametri[1] + '): ' + parametri[3];
        */
        var uporabnik = parametri[1];
        console.log(uporabnik);
        var vzdevek = parametri[3];
        var uporabnikovVzdevek = {uporabnik:uporabnik, vzdevek:vzdevek};
        this.dodajVzdevek(uporabnikovVzdevek);
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    case 'barva':
      besede.shift();
      var barva = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.spremeniBarvo(barva);
      break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};